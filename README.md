# aws_autoscaling


## Description
This project allows to deploy terraform script via docker runner. Remote state is hosted by gitlab, so can be accessible anyone with correct permissions after ci/cd configuring. Applying script is available only from main branch and in case of merch request maintainer will be able to see changes which terraform will try to make. 
Current terraform script deploy auto scaling group in aws based on cloudwatch metric rules. 

