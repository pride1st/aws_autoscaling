provider "aws" {
    region     = "eu-west-3"
}

data "aws_ami" "amazon_linux" {
  most_recent = true
  owners = ["137112412989"]
  filter {
    name = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-x86_64-gp2"]
  }

}

data "aws_availability_zones" "available" {}




resource "aws_security_group" "for_auto" {
      
  dynamic "ingress"{
    for_each = ["80", "443", "22"]
   content {
    
    from_port        = ingress.value
    to_port          = ingress.value
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    
   }
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }

  tags = {
    Name = "for_auto_scaling_group_from_terraform"
  }
}



resource "aws_launch_configuration" "for_scaling" {

  name_prefix     = "from_terraform_configuration_"
  image_id = data.aws_ami.amazon_linux.id
  security_groups = [aws_security_group.for_auto.id]
  instance_type = "t2.micro"
  root_block_device {
    volume_size = 25 
    volume_type = "gp2"
  }
  user_data = <<EOF
  #!/bin/bash
yum -y install httpd
echo "<h2>WebServer with IP: `hostname -i`</h2><br>Build by Terraform!"  >  /var/www/html/index.html
sudo service httpd start
chkconfig httpd on
EOF

    lifecycle {
    create_before_destroy = true
  }
}



resource "aws_autoscaling_group" "test_group" {
  
  name = "ter_test"
  max_size                  = 3
  min_size                  = 2
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = 2
  force_delete              = false
  launch_configuration      = aws_launch_configuration.for_scaling.name
  load_balancers = [aws_elb.web.id]
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]

  lifecycle {
create_before_destroy = true
  }
  tag {
      key                 = "Name"
      value               = "auto_scal_inst"
      propagate_at_launch = true
    }

}


resource "aws_elb" "web" {

  security_groups    = [aws_security_group.for_auto.id]
  name               = "web-elb-ter"
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  
  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
  
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 30
  }
  tags = {
    Name = "elb_from_terraform"
  }
}



resource "aws_autoscaling_policy" "web_policy_up" {
  name = "web_policy_up"
  scaling_adjustment = 1
  adjustment_type = "ChangeInCapacity"
  cooldown = 30
  autoscaling_group_name = aws_autoscaling_group.test_group.name
}
resource "aws_cloudwatch_metric_alarm" "cpu_alarm_up" {
  alarm_name = "cpu_alarm_up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "70"
dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.test_group.name
  }
alarm_description = "This metric monitor EC2 instance CPU utilization"
  alarm_actions = [ aws_autoscaling_policy.web_policy_up.arn ]
}
resource "aws_autoscaling_policy" "web_policy_down" {
  name = "web_policy_down"
  scaling_adjustment = -1
  adjustment_type = "ChangeInCapacity"
  cooldown = 30
  autoscaling_group_name = aws_autoscaling_group.test_group.name
}
resource "aws_cloudwatch_metric_alarm" "cpu_alarm_down" {
  alarm_name = "cpu_alarm_down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "30"
dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.test_group.name
  }
alarm_description = "This metric monitor EC2 instance CPU utilization"
  alarm_actions = [ aws_autoscaling_policy.web_policy_down.arn ]
}




output "web_dns" {
  value = aws_elb.web.dns_name
}


